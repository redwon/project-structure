File structure:

common/
  Have common styles for all pages, include base styles.

  components/
    Have a single LESS file for each component.

  layout/
    Styles for header, footer etc.

  mixins/
    Have a shared mixins.

  vendors/
    Have 3rd party css.

pages/
  Have a single LESS file for each specific/custom page.

shared/
  Have shared LESS files between compiled files.


Files available in the design center:

theme.less
base.less
header.less
footer.less

Compiled files:

common / main.less
and an attached file to the page of specific / custom pages.

Output files on page:
The page have two files main.less and less for the current page.

Rules:

when editing one of base.less, header.less, footer.less
compile main.less.

when editing the attached file to the page
compile only this file.

when editing theme.less
compile main.less and all linked files to pages.
