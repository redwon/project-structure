var throttle = function(func, limit) {
  var lastFunc;
  var lastRan;

  return function() {
    var context = this;
    var args = arguments;

    if (!lastRan) {
      func.apply(context, args);
      lastRan = Date.now();
    } else {
      clearTimeout(lastFunc);
      lastFunc = setTimeout(function() {
        if (Date.now() - lastRan >= limit) {
          func.apply(context, args);
          lastRan = Date.now();
        }
      }, limit - (Date.now() - lastRan));
    }
  };
};

var lazyLoadImages = new LazyLoadImagesClass().init();

function LazyLoadImagesClass(selector) {
  var _self = this;
  this.selector = selector ? selector : 'img.lazy-image';
  this.images = [];

  this.init = function() {
    var elements = document.querySelectorAll(this.selector);
    if (!elements.length) return;

    for (var i = 0; i < elements.length; i++) {
      this.images.push(elements[i]);
    }

    this.checkViewport();
    this.addListeners();
  };

  this.addListeners = function() {
    window.addEventListener('scroll', this.listener, false);
    window.addEventListener('resize', this.listener, false);
  };

  this.removeListeners = function() {
    window.removeEventListener('scroll', this.listener, false);
    window.removeEventListener('resize', this.listener, false);
  };

  this.listener = throttle(function(e) {
    _self.checkViewport();
  }, 300);

  this.checkViewport = function() {
    for (var i = 0; i < this.images.length; i++) {
      if (this.elementInViewport(this.images[i])) {
        this.replaceSrc(this.images[i]);
        this.images.splice(i, 1);
        i--;
      }
    }

    if (this.images.length === 0) this.removeListeners();
  };

  this.elementInViewport = function(el) {
    var rect = el.getBoundingClientRect();
    var vertOffset = rect.height;
    var windowHeight = window.innerHeight || document.body.clientHeight;
    var windowWidth = window.innerWidth || document.body.clientWidth;
    var inVertView =
      rect.top - vertOffset <= windowHeight && rect.top + rect.height + vertOffset >= 0;
    var inHorView = rect.left <= windowWidth && rect.left + rect.width >= 0;

    return inVertView && inHorView;
  };

  this.replaceSrc = function(img) {
    var imgSrc = img.getAttribute('data-src');
    img.setAttribute('src', imgSrc);
  };
}

// Common script

// Main navigation
function MainNavTool() {
  var _self = this;
  this.selector = {
    navigation: '.main-nav',
    trigger: '.main-nav-bars',
    hideTrigger: '.main-nav__close, .main-nav-lock-content',
    navItems: '.main-nav .has_submenu',
    lockContent: '.main-nav-lock-content',
    toggle: 'main_nav_show',
    subNavToggle: 'show_sub_menu'
  };

  this.init = function() {
    this.$navigation = $(this.selector.navigation);
    this.$trigger = $(this.selector.trigger);
    this.$hideTrigger = $(this.selector.hideTrigger);
    this.$navItems = $(this.selector.navItems);
    this.$lockContent = $(this.selector.lockContent);
    this._addListeners();

    return this;
  };

  this._addListeners = function() {
    this.$trigger.on('click', function(e) {
      e.preventDefault();
      _self.show();
    });

    this.$hideTrigger.on('click', function(e) {
      e.preventDefault();
      _self.hide();
    });

    this.$navItems.on('click', function(e) {
      e.preventDefault();
      e.stopPropagation();
      $(this).toggleClass(_self.selector.subNavToggle);
    });
  };

  this.show = function() {
    this.$navigation.add(this.$lockContent).addClass(this.selector.toggle);
  };

  this.hide = function() {
    this.$navigation.add(this.$lockContent).removeClass(this.selector.toggle);
  };
}

var mainNavTool = new MainNavTool().init();

// Recent product page

// Product filter
function ProductsFilter() {
  var _self = this;
  this.duration = 400;
  this.selector = {
    groupName: '.rp-filter__group-name',
    group: '.rp-filter__list',
    title: '.rp-filter__title',
    groups: '.rp-filter__groups',
    groupToggle: 'group_hide',
    filterToggle: 'mobile_show'
  };

  this.init = function() {
    this.$groupName = $(this.selector.groupName);
    this.$title = $(this.selector.title);
    this._addListeners();

    return this;
  };

  this._addListeners = function() {
    this.$groupName.on('click', function(e) {
      e.preventDefault();
      var $group = $(this).next(_self.selector.group);

      if ($(this).hasClass(_self.selector.groupToggle)) {
        $(this).removeClass(_self.selector.groupToggle);
        _self.show($group);
      } else {
        $(this).addClass(_self.selector.groupToggle);
        _self.hide($group);
      }
    });

    this.$title.on('click', function(e) {
      e.preventDefault();
      $(this)
        .toggleClass(_self.selector.filterToggle)
        .next(_self.selector.groups)
        .toggleClass(_self.selector.filterToggle);
    });
  };

  this.show = function($group) {
    $group.stop().slideDown(_self.duration);
  };

  this.hide = function($group) {
    $group.stop().slideUp(_self.duration);
  };
}

var productsFilter = new ProductsFilter().init();

Vue.component('pd-accordion', {
  template: '#pd-accordion-template',
  data() {
    return {
      isOpen: false
    };
  },

  props: {
    title: String,
    open: Boolean
  },

  mounted() {
    if (this.open) {
      this.isOpen = true;
    }
  },

  methods: {
    enter(element) {
      const { height } = getComputedStyle(element);
      element.style.height = 0;

      setTimeout(() => {
        element.style.height = height;
      });
    },
    afterEnter(element) {
      element.style.height = 'auto';
    },
    leave(element) {
      const { height } = getComputedStyle(element);
      element.style.height = height;

      setTimeout(() => {
        element.style.height = 0;
      });
    }
  }
});

Vue.component('pd-price', {
  template: '#pd-price-template',
  data() {
    return {
      current: null
    };
  },

  inject: ['$validator'],

  props: {
    prefix: String,
    value: Number,
    rules: Object,
    fixed: {
      type: Array,
      default: () => []
    }
  },

  mounted() {
    this.current = this.value;
  },

  computed: {
    minPrice() {
      return this.prefix + this.rules.min_value;
    },
    maxPrice() {
      return this.prefix + this.rules.max_value;
    },
    validationError() {
      return `Enter an value between ${this.minPrice} and ${this.maxPrice}`;
    },
    isValid() {
      return this.current >= this.rules.min_value && this.current <= this.rules.max_value;
    }
  },

  methods: {
    select(value) {
      this.current = value;
      this.$emit('input', value);
      this.reset();
      this.validate();
    },
    enter(event) {
      let value = parseFloat(event.target.value);
      value = isNaN(value) ? 0 : value;
      this.current = value;
      this.$emit('input', value);
      this.validate();
    },
    reset() {
      this.$refs.input.value = '';
    },
    validate() {
      return this.isValid;
    }
  }
});

Vue.component('egift-delivery-details', {
  template: '#egift-delivery-details-template',
  data() {
    return {
      recipientName: {
        value: '',
        rules: {
          required: true,
          english_alpha: true,
          max: 20
        }
      },
      recipientEmail: {
        value: '',
        rules: {
          required: true,
          email: true
        }
      },
      confirmRecipientEmail: {
        value: '',
        rules: {
          required: true,
          confirmed: 'recipientEmail'
        }
      },
      senderName: {
        value: '',
        rules: {
          required: true,
          english_alpha: true,
          max: 22
        }
      },
      recipientMessage: {
        value: '',
        rules: {
          english_alpha: true,
          max: 150
        }
      },
      deliveryDate: {
        value: '',
        rules: {
          required: true,
          date_format: 'dd/MM/yyyy'
        }
      }
    };
  },

  inject: ['$validator']
});

function containsEnglishAlphaSpace(value) {
  const regex = /^[a-z ]+$/gi;
  return regex.test(value);
}

Vue.use(VeeValidate, { inject: false });

const productDetailsPage = new Vue({
  el: '#product-details-page',
  data() {
    return {
      self: false,
      price: {
        value: 25,
        prefix: '$',
        range: [25, 50, 100, 150],
        rules: {
          min_value: 25,
          max_value: 500
        }
      },
      quantity: {
        value: 1,
        rules: {
          required: true,
          numeric: true,
          min_value: 1,
          max_value: 99
        }
      }
    };
  },

  $_veeValidate: {
    validator: 'new'
  },

  mounted() {
    this.$validator.extend('english_alpha', {
      getMessage: field => `The ${field} may only contain english letters.`,
      validate: value => containsEnglishAlphaSpace(value)
    });
  },

  methods: {
    onSubmit() {
      this.$validator.validate().then(valid => {
        if (valid && this.$refs.price.validate()) {
          this.submitData();
        }
      });
    },
    submitData() {
      console.log('Submit data');
    }
  }
});
